# python-signal-bot

Framework to easily write bots for messenger Signal

This project is based on [signal-cli](https://github.com/AsamK/signal-cli) and it's usage is structurally inspired by [python-telegram-bot](https://python-telegram-bot.org/).

It's unclear if this will really be developed further in the future. It's currently serving a practical purpose for me and I don't know if I'll be able to invest the time and effort to develop it further with documentation, tests etc.
